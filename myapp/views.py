from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from .serializers import PersonSerializer
from .models import Person


# Create your views here.
@csrf_exempt
@api_view(['GET', 'POST'])
def person(request):
    if request.method == 'GET':
        types = Person.objects.all()
        serializer = PersonSerializer(types, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = PersonSerializer(data=request.query_params)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=201)
        return Response(serializer.data,status=400)
